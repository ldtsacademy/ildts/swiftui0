//
//  ContentView.swift
//  swiftui0
//
//  Created by Josemaria Carazo Abolafia on 11/5/21.
//

import SwiftUI

struct ContentView: View {

    //Es esencial que sea @State para que los usos que se hacen de la variable se actualicen cuando su valor cambie.
    @State var tapnumber :Int = 0

    var body: some View {
        //Toda estructura tipo View tiene un atributo body donde se encuentran los elementos que queremos poner en la pantalla.
        //Usa el + que hay arriba a la derecha para añadir más cosas a la pantalla.

        Section {
            Label("SwiftUI Demo-0", systemImage: "iphone")
                .font(/*@START_MENU_TOKEN@*/.headline/*@END_MENU_TOKEN@*/)
            Label("by LDTS", systemImage: "signature")
                .font(.subheadline).padding()
        }
        
        
        Divider().padding(.top, 21.0).padding(.bottom, 40.0)
        
        //Gracias a que tapnumber es una variable @State este texto se actualizará cuando cambie su valor
        Text("Number of taps: \(tapnumber)")
            .foregroundColor(Color.yellow)
            .padding()
            .font(/*@START_MENU_TOKEN@*/.title3/*@END_MENU_TOKEN@*/)
        
        Divider().frame(height: 10.0).foregroundColor(.white).padding(30)

        Button("Tap again!") {
            tapnumber = tapnumber + 1
        }.padding()
        .frame(width: 250.0)
        .foregroundColor(.black)
        .background(Color(red: 252/255, green: 1, blue: 252/255, opacity: 1.0))
        .clipShape(Rectangle())
        .border(Color.white, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
        .cornerRadius(/*@START_MENU_TOKEN@*/3.0/*@END_MENU_TOKEN@*/)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
